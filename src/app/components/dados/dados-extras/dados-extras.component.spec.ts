import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosExtrasComponent } from './dados-extras.component';

describe('DadosExtrasComponent', () => {
  let component: DadosExtrasComponent;
  let fixture: ComponentFixture<DadosExtrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosExtrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosExtrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
