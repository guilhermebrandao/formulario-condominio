import { Empresa } from '../../../shared/models/empresa';
import { EmpresaComArt } from '../../../shared/models/empresa-com-art';

export class DadosExtras {
    temObraCivil: boolean; // Obra civil, reformas ou manutenção predial.
    temAdministradora: boolean;
    temVigilancia: boolean; // Vigilancia, portaria, zeladoria ou de fornecimento de mão de obra.
    temDedetizacao: boolean; // Dedetização geral, desinfecção, higienização, desratização.
    temManutElevador: boolean; // Manutenção de elevadores.
    temSistemaSegurancaEletronica: boolean;
    temManutPortaoAut: boolean; // Cancelas/Portões automáticos.
    temManutTelInterfone: boolean; // Manutenção de central telfônica/interfone.
    temManutCentralRedeGas: boolean; // Manutenção de central/rede de gás.
    temManutSubestacaoEletrica: boolean; // Manutenção de subestação elétrica ou grupo gerador de energia.
    temManutEquipCombIncendio: boolean; // Manutenção de equipamentos de combate a incêndio.
    temManutParaRaio: boolean; // Manutenção de sistema de proteção contra descarga atmosférica (para-raio).
    temManutPocos: boolean; //

    empresas: Empresa[];
    empresasComArt: EmpresaComArt[]; 




}
