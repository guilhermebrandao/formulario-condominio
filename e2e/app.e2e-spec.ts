import { FormularioCondominioPage } from './app.po';

describe('formulario-condominio App', () => {
  let page: FormularioCondominioPage;

  beforeEach(() => {
    page = new FormularioCondominioPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
