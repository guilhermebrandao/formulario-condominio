import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cmcMask'
})
export class CmcMaskPipe implements PipeTransform {

  transform(value: string): string {
    if (value !== undefined)
    return value.substr(0,1) + '.' + value.substr(1,6) + '.' + value.substring(6) ;
  }

}
