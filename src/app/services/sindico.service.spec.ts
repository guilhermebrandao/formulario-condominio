import { TestBed, inject } from '@angular/core/testing';

import { SindicoService } from './sindico.service';

describe('SindicoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SindicoService]
    });
  });

  it('should be created', inject([SindicoService], (service: SindicoService) => {
    expect(service).toBeTruthy();
  }));
});
