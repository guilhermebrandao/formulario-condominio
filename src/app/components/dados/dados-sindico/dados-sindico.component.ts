import { Component, OnInit } from '@angular/core';

import { SindicoService } from '../../../services/sindico.service';
import { Sindico } from './sindico';

@Component({
  selector: 'app-dados-sindico',
  templateUrl: './dados-sindico.component.html',
  styleUrls: ['./dados-sindico.component.css']
})
export class DadosSindicoComponent {

  private sindico: Sindico;

  constructor(private sindicoService: SindicoService) {
    this.sindico = new Sindico();
  }

  insertSindico() {
    if (this.sindico.nome != null) {
      this.sindicoService.insert(this.sindico).subscribe(dados => {
        console.log(this.sindico);
      });
    }
  }

}
