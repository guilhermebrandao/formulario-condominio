import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DadosModule } from './components/dados/dados.module';
import { InstrucoesComponent } from './components/instrucoes/instrucoes.component';

@NgModule({
  declarations: [
    AppComponent,
    InstrucoesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    DadosModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
