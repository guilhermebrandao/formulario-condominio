import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule }  from '@angular/router';

import { DadosCondominioComponent } from './dados-condominio/dados-condominio.component';
import { DadosSindicoComponent } from './dados-sindico/dados-sindico.component';
import { DadosExtrasComponent } from './dados-extras/dados-extras.component';

import { CondoService } from '../../services/condo.service';
import { SindicoService } from '../../services/sindico.service';

import { CmcMaskPipe } from '../../shared/pipes/cmc-mask.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [
    DadosCondominioComponent,
    DadosSindicoComponent,
    DadosExtrasComponent,
    CmcMaskPipe
  ],
  exports: [
    DadosCondominioComponent,
    DadosSindicoComponent,
    DadosExtrasComponent
  ],
  providers: [CondoService, SindicoService]
})
export class DadosModule { }
