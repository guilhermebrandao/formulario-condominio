import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'Rxjs/rx';

import { Sindico } from '../components/dados/dados-sindico/sindico';

@Injectable()
export class SindicoService {

  private apiURL: string = 'http://condoform-api.herokuapp.com/';

  constructor(private http: Http) { }

  insert(sindico: Sindico) {

    const url = `${this.apiURL}updateCondo`;

    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http
      .post(url, JSON.stringify(sindico), options)
      .map(res => {
        if (res.text()) {
          console.log(res.text());
        }
      });
  }
}
