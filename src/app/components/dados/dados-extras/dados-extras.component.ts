import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Pergunta } from './pergunta';

@Component({
  selector: 'app-dados-extras',
  templateUrl: './dados-extras.component.html',
  styleUrls: ['./dados-extras.component.css']
})
export class DadosExtrasComponent implements OnInit {

  @Input() multiple: boolean = false;

  perguntas: Pergunta[] = [
      new Pergunta(0, 'ObraCivil', '3.1 - Estão sendo realizadas obras civis, reformas ou manutenção predial do Condomínio?', true),
      new Pergunta(1, 'Administradora', '3.2 - Tem empresa de administração de Condomínio contrada?', false),
      new Pergunta(2, 'Vigilancia', '3.3 - Tem empresa de serviços de vigilância, portaria, zeladoria ou de fornecimento de mão de obra contratada?', false),
      new Pergunta(3, 'Dedetizacao', '3.4 - Tem empresa de dedetização em geral, desinfecção, higienização ou desratização contrata?', false),
      new Pergunta(4, 'ManutElevador', '3.5 - Tem empresa de manutenção de elevadores contratada?', true),
      new Pergunta(5, 'SistemaSegurancaEletronica', '3.6 - Tem empresa de manutenção de sistema de segurança eletrônica contrada?', true),
      new Pergunta(6, 'ManutPortaoAut', '3.7 - Tem empresa de manutenção de cancelas e/ou portôes automáticos contratada?', true),
      new Pergunta(7, 'ManutTelInterfone', '3.8 - Tem empresa de manutenção de central telefônica e/ou de interfone?', true),
      new Pergunta(8, 'ManutCentralRedeGas', '3.9 - Tem empresa de manutenção de central e/ou rede de gás contratada?', true),
      new Pergunta(9, 'ManutSubestacaoEletrica', '3.10 - Tem empresa de manutenção de subestação elétrica ou grupo gerador de energia contratada?', true),
      new Pergunta(10, 'ManutEquipCombIncendio', '3.11 - Tem empresa de manutenção de equipamentos de combate a incêndio contratada?', true),
      new Pergunta(11, 'ManutParaRaio', '3.12 - Tem empresa de manutenção de sistema de proteção contra descarga atmosféria (para-rios) contratada?', true),
      new Pergunta(12, 'ManutPocos', '3.13 - Temempresa de perfuração ou manutenção de poços contratada?', true)
      ];

  constructor(private el: ElementRef) { }

  ngOnInit() {
  }

  upload() {
        let inputEl = this.el.nativeElement.firstElementChild;
        // if (inputEl.files.length === 0) { return; };

        let files: FileList = inputEl.files;

        // const formData = new FormData();
        // for (let i = 0; i < files.length; i++) {
        //     formData.append(files[i].name, files[i]);
        // }

        console.log(files);
    }
}
