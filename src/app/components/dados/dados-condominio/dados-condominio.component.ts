import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { CondoService } from '../../../services/condo.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';

import { DadosCondominio } from './dados-condominio';

@Component({
  selector: 'app-dados-condominio',
  templateUrl: './dados-condominio.component.html',
  styleUrls: ['./dados-condominio.component.css']
})
export class DadosCondominioComponent implements OnInit {

  private dadosCondominio: DadosCondominio;

  constructor(private condoService: CondoService) {
    this.dadosCondominio = new DadosCondominio();
  }

  ngOnInit() {
  }

  searchCondo(senha: string) {
    this.condoService.getCondoInfo(senha)
      .then(res => this.dadosCondominio = res)
      .catch(error => {
        console.log(error);
      })
  }

  onSubmit() {
    if (this.dadosCondominio.id !== null) {
      this.condoService.updateCondo(this.dadosCondominio).subscribe(dados => {
        console.log("Angular funcionando.");
      });
    }
  }

}
