export class DadosCondominio {
  id: number;
  cnpj: string;
  cmc: string; // Inscrição municipal
  nome: string; // Nome do prédio
  telfixo?: string;
  telcel?: string;
  email?: string;
}
