import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosCondominioComponent } from './dados-condominio.component';

describe('DadosCondominioComponent', () => {
  let component: DadosCondominioComponent;
  let fixture: ComponentFixture<DadosCondominioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosCondominioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosCondominioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
