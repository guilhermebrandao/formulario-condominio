import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosSindicoComponent } from './dados-sindico.component';

describe('DadosSindicoComponent', () => {
  let component: DadosSindicoComponent;
  let fixture: ComponentFixture<DadosSindicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosSindicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosSindicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
