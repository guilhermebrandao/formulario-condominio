# README #

### What is this repository for? ###

* Repositório para os códigos referentes ao projeto em Angular para criação do Formulário Eletrônico de Condomínios proposto pela Prefeitura da Cidade do Recife

### How do I get set up? ###

Usando o gerenciador de pacotes, digitar no console (cmd windows) {DIRETORIO}/formulario-condominio/

* npm install 

Para rodar o projeto, digitar no console (cmd windows) {DIRETORIO}/formulario-condominio/

* ng serve