import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instrucoes',
  templateUrl: './instrucoes.component.html',
  styleUrls: ['./instrucoes.component.css']
})
export class InstrucoesComponent implements OnInit {

  titulo: string = 'INSTRUÇÕES';

  linha1: string = '- O preenchimento é obrigatório e o envio no prazo de até     /     / 2017;';
  linha2: string = '- Antes de iniciar o preenchimento certifique-se de que está com todos os documentos solicitados no Termo de Intimação;';
  linha3: string = '- A falta de atendimento importará em multa de R$ 703,74 a R$ 14.074,95;';
  linha4: string = '- Abreviaturas utilizadas: ART - Anotação de Responsabilidade Técnica; e CREA-PE Conselho Regional de Engenharia e Agronomia de Pernambuco;';

  linhas: string[] = [this.linha1, this.linha2, this.linha3, this.linha4 ];
  constructor() { }

  ngOnInit() {
  }

}
