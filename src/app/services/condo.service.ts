import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'Rxjs/rx';
import 'rxjs/add/operator/toPromise';

import { DadosCondominio } from '../components/dados/dados-condominio/dados-condominio';

@Injectable()
export class CondoService {

  private apiURL: string = 'http://condoform-api.herokuapp.com/';
  constructor(private http: Http) { }

  getCondoInfo(senha: string): Promise<DadosCondominio> {

    const url = `${this.apiURL}condo/${senha}`;

    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http.get(url, options)
      .toPromise()
      .then(response => response.json().data as DadosCondominio)
      .catch(this.handleError); 
  }

  updateCondo(dadosCondominio: DadosCondominio) {

    const url = `${this.apiURL}updateCondo/${dadosCondominio.id}`;

    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http
      .put(url, JSON.stringify(dadosCondominio), options)
      .map(res => {
        if (res.text()) {
          console.log(res.text());
        }
      });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
